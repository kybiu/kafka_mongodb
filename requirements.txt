certifi==2020.12.5
dnspython==1.16.0
kafka-python==2.0.2
pymongo==3.11.2
python-dotenv==0.15.0
