from kafka import KafkaConsumer, KafkaProducer, BrokerConnection
from pymongo import MongoClient
from json import loads
from datetime import datetime, timedelta
import os
from dotenv import load_dotenv
import _thread

load_dotenv()
MONGO_CLIENT = os.environ.get("MONGO_CLIENT")
MONGO_USER = os.environ.get("MONGO_USER")
MONGO_PASSWORD = os.environ.get("MONGO_PASSWORD")
MONGO_DATABASE = os.environ.get("MONGO_DATABASE")
MONGO_COLLECTION = os.environ.get("MONGO_COLLECTION")

BROKER = os.environ.get("BROKER")
USER = os.environ.get("BROKER_USER")
PASSWORD = os.environ.get("BROKER_PASSWORD")


def read_topic_and_insert_mongo(topic, shop_id):
    client = MongoClient(MONGO_CLIENT,
                         username=MONGO_USER,
                         password=MONGO_PASSWORD)
    db = client[MONGO_DATABASE]
    collection = db[MONGO_COLLECTION]

    consumer = KafkaConsumer(
        topic,
        bootstrap_servers=[BROKER],
        security_protocol='SASL_PLAINTEXT',
        sasl_mechanism='PLAIN',
        sasl_plain_username=USER,
        sasl_plain_password=PASSWORD,
        auto_offset_reset="earliest",
        # auto_offset_reset="latest",
        # enable_auto_commit=True,
        # group_id='ducanh_group',
        # consumer_timeout_ms=2000,
        value_deserializer=lambda x: loads(x.decode('utf-8'))
    )
    for msg in consumer:
        now = datetime.now()
        hours_added = timedelta(hours=7)
        current_time = now + hours_added

        msg_value = msg.value
        msg_value["insert_time"] = current_time
        msg_value["shop_id"] = shop_id
        print(msg_value)

        collection.insert(msg_value)
        # consumer.commit()


if __name__ == '__main__':
    # start thread shop gau
    shop_gau_topic = "bn_rasa_events_test"
    shop_gau_id = 1454523434857990
    _thread.start_new_thread(read_topic_and_insert_mongo, (shop_gau_topic, shop_gau_id,))

    # start thread kscloset
    kscloset_topic = "ks_rasa_events"
    kscloset_id = 1059845080701224
    _thread.start_new_thread(read_topic_and_insert_mongo, (kscloset_topic, kscloset_id,))

    while 1:
        pass